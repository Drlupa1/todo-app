FROM node:16-alpine

WORKDIR /app

COPY package.json package-lock.json* ./

RUN npm ci && npm cache clean --force 

COPY ./src ./src

EXPOSE 3000

ENTRYPOINT ["npm"]

CMD ["run-script", "dev"]
